# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Picture-in-picture
pictureinpicture-pause =
    .aria-label = Pauzeren
pictureinpicture-play =
    .aria-label = Afspelen
pictureinpicture-mute =
    .aria-label = Dempen
pictureinpicture-unmute =
    .aria-label = Dempen opheffen
pictureinpicture-unpip =
    .aria-label = Terugsturen naar tabblad
pictureinpicture-close =
    .aria-label = Sluiten
pictureinpicture-subtitles-label = Ondertitels
pictureinpicture-font-size-label = Lettergrootte
pictureinpicture-font-size-small = Klein
pictureinpicture-font-size-medium = Normaal
pictureinpicture-font-size-large = Groot
