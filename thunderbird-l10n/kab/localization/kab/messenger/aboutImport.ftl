# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Kter
export-page-title = Sifeḍ

## Header

import-start = Kter ifecka
import-from-app = Kter seg usnas
import-file = Kter-d seg ufaylu
export-profile = Sifeḍ

## Buttons

button-back = Ɣer deffir
button-continue = Kemmel
button-export = Sifeḍ

## Import from app steps

app-name-thunderbird = Thunderbird
app-name-seamonkey = SeaMonkey
app-name-outlook = Outlook
app-name-becky = Becky! Internet Mail
app-name-apple-mail = Apple Mail

## Import from file selections

file-calendar = Kter iwitayen
file-addressbook = Kter imedlisen n tansiwin

## Import from app profile steps

items-pane-checkbox-accounts = Imiḍanen d Yiɣewwaren
items-pane-checkbox-address-books = Imedlisen n tensa
items-pane-checkbox-calendars = Iwitayen
items-pane-checkbox-mail-messages = Iznan n tirawt

## Import from address book file steps

addr-book-csv-file = Afaylu iferqen s tefrayin neɣ s tebzimin (.csv, .tsv)
addr-book-ldif-file = Afaylu n LDIF (.ldif)
addr-book-vcard-file = Afaylu n vCard (.vcf, .vcard)
addr-book-sqlite-file = Afaylu n taffa n yisefka n SQLite (.sqlite)
addr-book-mab-file = Afaylu n taffa n yisefka s umasal Mork (.mab)
addr-book-file-picker = Fren afaylu n yimedlis n tensa
addr-book-csv-field-map-title = Semṣadi gar yismawen n wurti
addr-book-csv-field-map-desc = Fren urtiyen n umedlis n tansiwin yemṣaddan d wurtiyen iɣbual. Kkes aṛcam i wurtiyen ur tebɣiḍ ara ad d-tketreḍ.
addr-book-directories-pane-source = Afaylu aɣbalu:

## Import from calendar file steps

import-from-calendar-file-desc = Fren afaylu iCalendar (.ics) i tebɣiḍ ad d-tketreḍ.
calendar-items-loading = Asali n yiferdisen...
calendar-items-filter-input =
    .placeholder = Iferdisen n imsizdig…
calendar-select-all-items = Fren akk
calendar-deselect-all-items = Kkes akk afran

## Import dialog

error-pane-title = Tuccḍa
error-message-failed = Aktar yecceḍ, d ayen ur nettwarǧa ara. Ugar n telɣut tezmer ad tili deg Console n tuccḍa.
error-failed-to-parse-ics-file = Ulac iferdisen ara yettwaktaren deg ufaylu.
error-export-failed = Asifeḍ yecceḍ, d ayen ur nettwarǧa ara. Ugar n telɣut tezmer ad tili deg Console n tuccḍa.
error-message-no-profile = Ulac amaɣnu yettwafen.

## <csv-field-map> element

csv-first-row-contains-headers = Izirig amezwaru yegber ismawen n wurtan
csv-source-field = Urti aɣbalu
csv-source-first-record = Asekles amezwaru
csv-source-second-record = Asekles wis sin
csv-target-field = Urtan n imedlis n tensa

## Export tab

export-profile-title = Sifeḍ imiḍanen n yimayl, iznan n yimayl, imedlisen n tansiwin akked yiɣewwaren s ufaylu Zip.
export-open-profile-folder = Ldi akaram n umaɣnu
export-brand-name = { -brand-product-name }

## Summary pane


## Footer area


## Step navigation on top of the wizard pages

step-confirm = Sentem
# Variables:
# $number (number) - step number
step-count = { $number }
