# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

chat-title = Pratekontoar
chat-table-heading-account = ID
chat-table-heading-protocol = Protokoll
chat-table-heading-name = Namn
chat-table-heading-actions = Handlingar
