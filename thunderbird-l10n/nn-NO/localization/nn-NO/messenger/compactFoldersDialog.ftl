# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

compact-dialog-window =
    .title = Komprimer mappene
    .style = width: 50em;
compact-dialog =
    .buttonlabelaccept = Komprimer no
    .buttonaccesskeyaccept = N
    .buttonlabelcancel = Minn meg på det seinare
    .buttonaccesskeycancel = M
    .buttonlabelextra1 = Les meir …
    .buttonaccesskeyextra1 = L
compact-dialog-never-ask-checkbox =
    .label = Komprimer mappene automatisk i framtida
    .accesskey = a
