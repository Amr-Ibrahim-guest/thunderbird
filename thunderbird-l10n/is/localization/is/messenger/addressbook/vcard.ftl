# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.


# Display Name

vcard-displayname = Birtingarnafn
vcard-displayname-placeholder =
    .placeholder = { vcard-displayname }

# Type selection

vcard-entry-type-label = Tegund
vcard-entry-type-home = Heima
vcard-entry-type-work = Vinna
vcard-entry-type-none = Ekkert
vcard-entry-type-custom = Sérsniðið

# N vCard field

vcard-name-header = Nafn
vcard-n-prefix = Forskeyti
vcard-n-add-prefix =
    .title = Bæta við forskeyti
vcard-n-firstname = Skírnarnafn
vcard-n-add-firstname =
    .title = Bæta við skírnarnafni
vcard-n-middlename = Millinafn
vcard-n-add-middlename =
    .title = Bæta við millinafni
vcard-n-lastname = Eftirnafn
vcard-n-add-lastname =
    .title = Bæta við eftirnafni
vcard-n-suffix = Viðskeyti
vcard-n-add-suffix =
    .title = Bæta við viðskeyti

# Email vCard field

vcard-email-header = Tölvupóstföng
vcard-email-add = Bæta við tölvupóstfangi
vcard-email-label = Tölvupóstfang
vcard-primary-email-label = Sjálfgefið

# URL vCard field

vcard-url-header = Vefsvæði
vcard-url-add = Bæta við vefsvæði
vcard-url-label = Vefsvæði

# Tel vCard field

vcard-tel-header = Símanúmer
vcard-tel-add = Bæta við símanúmeri
vcard-tel-label = Símanúmer

# TZ vCard field

vcard-tz-header = Tímabelti
vcard-tz-add = Bæta við tímabelti

# IMPP vCard field

vcard-impp-header = Spjallreikningar
vcard-impp-add = Bæta við spjallreikningi
vcard-impp-label = Spjallreikningur

# BDAY and ANNIVERSARY vCard field

vcard-bday-anniversary-header = Sérstakar dagsetningar
vcard-bday-anniversary-add = Bæta við sérstakri dagsetningu
vcard-bday-label = Fæðingardagur
vcard-anniversary-label = Afmæli
vcard-date-day = Dagur
vcard-date-month = Mánuður
vcard-date-year = Ár

# ADR vCard field

vcard-adr-header = Póstföng
vcard-adr-add = Bæta við póstfangi
vcard-adr-label = Póstfang
vcard-adr-delivery-label = Afhendingarmerking
vcard-adr-pobox = Pósthólf
vcard-adr-ext = Lengra heimilisfang
vcard-adr-street = Heimilisfang
# Or "Locality"
vcard-adr-locality = Borg
# Or "Region"
vcard-adr-region = Ríki/Hérað
# The term "ZIP code" only applies in USA. Most locales should use "Postal code" only.
vcard-adr-code = Póstnúmer
vcard-adr-country = Land

# NOTE vCard field

vcard-note-header = Athugasemdir
vcard-note-add = Bæta við athugasemd

# TITLE, ROLE and ORGANIZATION vCard fields

vcard-org-header = Skipulagslegir eignleikar
vcard-org-add = Bæta við skipulagslegum eignleikum
vcard-org-title = Titill
vcard-org-role = Hlutverk
vcard-org-org = Fyrirtæki/Stofnun
