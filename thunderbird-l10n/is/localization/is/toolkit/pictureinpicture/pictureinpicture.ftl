# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Mynd-í-mynd
pictureinpicture-pause =
    .aria-label = Í bið
pictureinpicture-play =
    .aria-label = Spila
pictureinpicture-mute =
    .aria-label = Hljóðlaus
pictureinpicture-unmute =
    .aria-label = Kveikja á hljóði
pictureinpicture-unpip =
    .aria-label = Senda aftur á flipa
pictureinpicture-close =
    .aria-label = Loka
pictureinpicture-subtitles-label = Skjátextar
pictureinpicture-font-size-label = Leturstærð
pictureinpicture-font-size-small = Lítil
pictureinpicture-font-size-medium = Miðlungs
pictureinpicture-font-size-large = Stór
