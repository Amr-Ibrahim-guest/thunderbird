# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = 画中画
pictureinpicture-pause =
    .aria-label = 暂停
pictureinpicture-play =
    .aria-label = 播放
pictureinpicture-mute =
    .aria-label = 静音
pictureinpicture-unmute =
    .aria-label = 恢复声音
pictureinpicture-unpip =
    .aria-label = 返回标签页
pictureinpicture-close =
    .aria-label = 关闭
pictureinpicture-subtitles-label = 字幕
pictureinpicture-font-size-label = 字号
pictureinpicture-font-size-small = 小
pictureinpicture-font-size-medium = 中
pictureinpicture-font-size-large = 大
