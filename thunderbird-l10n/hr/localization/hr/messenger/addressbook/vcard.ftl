# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.


# Display Name

vcard-displayname = Ime za prikaz
vcard-displayname-placeholder =
    .placeholder = { vcard-displayname }

# Type selection

vcard-entry-type-label = Vrsta
vcard-entry-type-home = Kuća
vcard-entry-type-work = Posao
vcard-entry-type-none = Ništa
vcard-entry-type-custom = Prilagođeno

# N vCard field

vcard-name-header = Naziv
vcard-n-prefix = Prefiks
vcard-n-add-prefix =
    .title = Dodaj prefiks
vcard-n-firstname = Ime
vcard-n-add-firstname =
    .title = Dodaj ime
vcard-n-middlename = Srednje ime
vcard-n-add-middlename =
    .title = Dodaj srednje ime
vcard-n-lastname = Prezime
vcard-n-add-lastname =
    .title = Dodaj prezime
vcard-n-suffix = Sufiks
vcard-n-add-suffix =
    .title = Dodaj sufiks

# Email vCard field

vcard-email-header = Adrese e-pošte
vcard-email-add = Dodaj adrese e-pošte
vcard-email-label = Adresa e-pošte
vcard-primary-email-label = Zadano

# URL vCard field

vcard-url-header = Web stranice
vcard-url-add = Dodaj web stranice
vcard-url-label = Web stranica

# Tel vCard field

vcard-tel-header = Brojevi telefona
vcard-tel-add = Dodaj broj telefona
vcard-tel-label = Broj telefona

# TZ vCard field

vcard-tz-header = Vremenska zona
vcard-tz-add = Dodaj vremensku zonu

# IMPP vCard field

vcard-impp-header = Računi razgovora
vcard-impp-add = Dodaj račun razgovora
vcard-impp-label = Račun razgovora

# BDAY and ANNIVERSARY vCard field

vcard-bday-anniversary-header = Posebni datumi
vcard-bday-anniversary-add = Dodaj poseban datum
vcard-bday-label = Rođendan
vcard-anniversary-label = Godišnjica
vcard-date-day = Dan
vcard-date-month = Mjesec
vcard-date-year = Godina

# ADR vCard field

vcard-adr-header = Adrese
vcard-adr-add = Dodaj adresu
vcard-adr-label = Adresa
vcard-adr-delivery-label = Oznaka za isporuku
vcard-adr-pobox = Poštanski sandučić
vcard-adr-ext = Proširena adresa
vcard-adr-street = Ulica
# Or "Locality"
vcard-adr-locality = Grad
# Or "Region"
vcard-adr-region = Država/pokrajina
# The term "ZIP code" only applies in USA. Most locales should use "Postal code" only.
vcard-adr-code = Poštanski broj
vcard-adr-country = Država

# NOTE vCard field

vcard-note-header = Bilješke
vcard-note-add = Dodaj bilješku

# TITLE, ROLE and ORGANIZATION vCard fields

vcard-org-header = Svojstva organizacije
vcard-org-add = Dodaj svojstva organizacije
vcard-org-title = Naslov
vcard-org-role = Uloga
vcard-org-org = Organizacija
