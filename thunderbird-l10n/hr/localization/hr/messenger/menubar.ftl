# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

toolbar-context-menu-menu-bar =
    .toolbarname = Traka izbornika
    .accesskey = T

## Tools Menu

menu-tools-settings =
    .label = Postavke
    .accesskey = e
menu-addons-and-themes =
    .label = Dodaci i teme
    .accesskey = a

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = Način rada za rješavanje problema…
    .accesskey = T
menu-help-exit-troubleshoot-mode =
    .label = Isključi način rada za rješavanje problema
    .accesskey = o
menu-help-more-troubleshooting-info =
    .label = Više informacija za rješavanje problema
    .accesskey = m

## Mail Toolbar

toolbar-junk-button =
    .label = Neželjena pošta
    .tooltiptext = Označi odabrane poruke kao neželjene
toolbar-not-junk-button =
    .label = Nije neželjena pošta
    .tooltiptext = Označite odabrane poruke kao neželjene
toolbar-delete-button =
    .label = Obriši
    .tooltiptext = Obrišite odabrane poruke ili mapu
toolbar-undelete-button =
    .label = Vrati obrisano
    .tooltiptext = Poništi brisanje odabranih poruka

## View

menu-view-repair-text-encoding =
    .label = Ispravi kodiranje teksta
    .accesskey = v

## View / Layout

menu-font-size-label =
    .label = Veličina fonta
    .accesskey = o
menuitem-font-size-enlarge =
    .label = Povećaj veličinu fonta
    .accesskey = i
menuitem-font-size-reduce =
    .label = Smanji veličinu fonta
    .accesskey = S
menuitem-font-size-reset =
    .label = Vrati veličinu fonta na zadanu
    .accesskey = r
mail-uidensity-label =
    .label = Zbijenost
    .accesskey = Z
mail-uidensity-compact =
    .label = Kompaktno
    .accesskey = K
mail-uidensity-normal =
    .label = Normalno
    .accesskey = N
mail-uidensity-touch =
    .label = Za dodir
    .accesskey = Z
menu-spaces-toolbar-button =
    .label = Alatna traka zbijenosti
    .accesskey = s

## File

file-new-newsgroup-account =
    .label = Račun interesnih grupa…
    .accesskey = n
