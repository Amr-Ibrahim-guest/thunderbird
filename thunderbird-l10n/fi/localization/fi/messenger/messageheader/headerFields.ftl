# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header lists

message-header-sender-list-name = Lähettäjä
message-header-newsgroups-list-name = Keskusteluryhmät

## Other message headers.
## The field-separator is for screen readers to separate the field name from the field value.

message-header-organization-field = Organisaatio<span data-l10n-name="field-separator">:</span>
message-header-subject-field = Aihe<span data-l10n-name="field-separator">:</span>
message-header-website-field = Verkkosivusto<span data-l10n-name="field-separator">:</span>
# An additional email header field that the user has chosen to display. Unlike
# the other headers, the name of this header is not expected to be localised
# because it is generated from the raw field name found in the email header.
#   $fieldName (String) - The field name.
message-header-custom-field = { $fieldName }<span data-l10n-name="field-separator">:</span>

##

message-header-address-in-address-book-icon2 =
    .alt = Osoitekirjassa
message-header-address-not-in-address-book-icon2 =
    .alt = Ei osoitekirjassa
message-header-address-not-in-address-book-button =
    .title = Tallenna tämä osoite osoitekirjaan
message-header-address-in-address-book-button =
    .title = Muokkaa yhteystietoa
message-header-field-show-more = Lisää
    .title = Näytä kaikki vastaanottajat
message-ids-field-show-all = Näytä kaikki
