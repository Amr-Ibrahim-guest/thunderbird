# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Kuva kuvassa
pictureinpicture-pause =
    .aria-label = Pysäytä
pictureinpicture-play =
    .aria-label = Toista
pictureinpicture-mute =
    .aria-label = Vaimenna ääni
pictureinpicture-unmute =
    .aria-label = Palauta ääni
pictureinpicture-unpip =
    .aria-label = Lähetä takaisin välilehteen
pictureinpicture-close =
    .aria-label = Sulje
pictureinpicture-subtitles-label = Tekstitykset
pictureinpicture-font-size-label = Kirjasinkoko
pictureinpicture-font-size-small = Pieni
pictureinpicture-font-size-medium = Keskikokoinen
pictureinpicture-font-size-large = Suuri
