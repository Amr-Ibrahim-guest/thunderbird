# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.


# Display Name

vcard-displayname = Num per mussar
vcard-displayname-placeholder =
    .placeholder = { vcard-displayname }

# Type selection

vcard-entry-type-label = Tip
vcard-entry-type-home = Privat
vcard-entry-type-work = Lavur
vcard-entry-type-none = Nagin
vcard-entry-type-custom = Persunalisà

# N vCard field

vcard-name-header = Num
vcard-n-prefix = Prefix
vcard-n-add-prefix =
    .title = Agiuntar in prefix
vcard-n-firstname = Prenum
vcard-n-add-firstname =
    .title = Agiuntar in prenum
vcard-n-middlename = Segund prenum
vcard-n-add-middlename =
    .title = Agiuntar in segund prenum
vcard-n-lastname = Num
vcard-n-add-lastname =
    .title = Agiuntar in num da famiglia
vcard-n-suffix = Suffix
vcard-n-add-suffix =
    .title = Agiuntar in suffix

# Email vCard field

vcard-email-header = Adressas dad e-mail
vcard-email-add = Agiuntar ina adressa dad e-mail
vcard-email-label = Adressa dad e-mail
vcard-primary-email-label = Standard

# URL vCard field

vcard-url-header = Websites
vcard-url-add = Agiuntar ina website
vcard-url-label = Website

# Tel vCard field

vcard-tel-header = Numers da telefon
vcard-tel-add = Agiuntar in numer da telefon
vcard-tel-label = Numer da telefon

# TZ vCard field

vcard-tz-header = Zona d'urari
vcard-tz-add = Agiuntar ina zona d'urari

# IMPP vCard field

vcard-impp-header = Contos da chat
vcard-impp-add = Agiuntar in conto da chat
vcard-impp-label = Conto da chat

# BDAY and ANNIVERSARY vCard field

vcard-bday-anniversary-header = Datas spezialas
vcard-bday-anniversary-add = Agiuntar ina data speziala
vcard-bday-label = Di da naschientscha
vcard-anniversary-label = Anniversari
vcard-date-day = Di
vcard-date-month = Mais
vcard-date-year = Onn

# ADR vCard field

vcard-adr-header = Adressas
vcard-adr-add = Agiuntar ina adressa
vcard-adr-label = Adressa
vcard-adr-delivery-label = Etichetta da spediziun
vcard-adr-pobox = Chascha postala
vcard-adr-ext = Adressa extendida
vcard-adr-street = Adressa postala
# Or "Locality"
vcard-adr-locality = Lieu
# Or "Region"
vcard-adr-region = Stadi/provinza
# The term "ZIP code" only applies in USA. Most locales should use "Postal code" only.
vcard-adr-code = Numer postal
vcard-adr-country = Pajais

# NOTE vCard field

vcard-note-header = Notizias
vcard-note-add = Agiuntar ina notizia

# TITLE, ROLE and ORGANIZATION vCard fields

vcard-org-header = Infurmaziuns da l'organisaziun
vcard-org-add = Agiuntar infurmaziuns da l'organisaziun
vcard-org-title = Titel
vcard-org-role = Rolla
vcard-org-org = Organisaziun
