# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

toolbar-context-menu-menu-bar =
    .toolbarname = Trav da menu
    .accesskey = m

## Tools Menu

menu-tools-settings =
    .label = Parameters
    .accesskey = e
menu-addons-and-themes =
    .label = Supplements e designs
    .accesskey = S

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = Modus per schliar problems…
    .accesskey = M
menu-help-exit-troubleshoot-mode =
    .label = Deactivar il modus per schliar problems
    .accesskey = o
menu-help-more-troubleshooting-info =
    .label = Dapli infurmaziuns per schliar problems
    .accesskey = m

## Mail Toolbar

toolbar-junk-button =
    .label = Nungiavischà
    .tooltiptext = Marcar ils messadis tschernids sco nungiavischads
toolbar-not-junk-button =
    .label = Betg nungiavischà
    .tooltiptext = Betg marcar ils messadis tschernids sco nungiavischads
toolbar-delete-button =
    .label = Stizzar
    .tooltiptext = Stizzar ils messadis tschernids u l'ordinatur
toolbar-undelete-button =
    .label = Revocar il stizzar
    .tooltiptext = Revocar l'eliminaziun dals messadis tschernids

## View

menu-view-repair-text-encoding =
    .label = Reparar la codaziun dal text
    .accesskey = c

## View / Layout

menu-font-size-label =
    .label = Grondezza da scrittira
    .accesskey = o
menuitem-font-size-enlarge =
    .label = Engrondir la grondezza da la scrittira
    .accesskey = E
menuitem-font-size-reduce =
    .label = Reducir la grondezza da la scrittira
    .accesskey = d
menuitem-font-size-reset =
    .label = Reinizialisar la grondezza da scrittira
    .accesskey = R
mail-uidensity-label =
    .label = Cumpactadad
    .accesskey = C
mail-uidensity-compact =
    .label = Cumpact
    .accesskey = u
mail-uidensity-normal =
    .label = Normal
    .accesskey = N
mail-uidensity-touch =
    .label = Touch
    .accesskey = T
menu-spaces-toolbar-button =
    .label = Trav d'utensils da locals
    .accesskey = s

## File

file-new-newsgroup-account =
    .label = Conto da gruppas da discussiun…
    .accesskey = n
