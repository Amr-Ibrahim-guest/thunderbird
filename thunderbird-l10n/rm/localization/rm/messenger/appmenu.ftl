# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## View / Layout

appmenu-font-size-value = Grondezza da scrittira
appmenuitem-font-size-enlarge =
    .tooltiptext = Engrondir la grondezza da la scrittira
appmenuitem-font-size-reduce =
    .tooltiptext = Reducir la grondezza da la scrittira
# Variables:
# $size (String) - The current font size.
appmenuitem-font-size-reset =
    .label = { $size }px
    .tooltiptext = Reinizialisar la grondezza da la scrittira
