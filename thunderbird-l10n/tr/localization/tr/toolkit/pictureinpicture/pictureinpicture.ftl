# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Görüntü içinde görüntü
pictureinpicture-pause =
    .aria-label = Duraklat
pictureinpicture-play =
    .aria-label = Oynat
pictureinpicture-mute =
    .aria-label = Sesi kapat
pictureinpicture-unmute =
    .aria-label = Sesi aç
pictureinpicture-unpip =
    .aria-label = Sekmeye geri gönder
pictureinpicture-close =
    .aria-label = Kapat
pictureinpicture-subtitles-label = Altyazı
pictureinpicture-font-size-label = Yazı tipi boyutu
pictureinpicture-font-size-small = Küçük
pictureinpicture-font-size-medium = Orta
pictureinpicture-font-size-large = Büyük
