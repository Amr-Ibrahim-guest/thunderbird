# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

openpgp-key-assistant-title = OpenPGP Anahtar Yardımcısı

## Encryption status

openpgp-key-assistant-recipients-issue-header = Şifrelenemiyor

## Resolve section

openpgp-key-assistant-key-fingerprint = Parmak izi
openpgp-key-assistant-key-source =
    { $count ->
        [one] Kaynak
       *[other] Kaynaklar
    }
openpgp-key-assistant-key-collected-attachment = e-posta eki
openpgp-key-assistant-key-collected-keyserver = anahtar sunucusu

## Discovery section

openpgp-key-assistant-discover-title = Çevrimiçi keşif devam ediyor.

## Dialog buttons

openpgp-key-assistant-issue-resolve-button = Çöz…
openpgp-key-assistant-view-key-button = Anahtarı göster…
openpgp-key-assistant-recipients-show-button = Göster
openpgp-key-assistant-recipients-hide-button = Gizle
openpgp-key-assistant-cancel-button = Vazgeç
openpgp-key-assistant-back-button = Geri dön
openpgp-key-assistant-accept-button = Kabul et
openpgp-key-assistant-close-button = Kapat
openpgp-key-assistant-disable-button = Şifrelemeyi kapat
openpgp-key-assistant-confirm-button = Şifreli gönder
# Variables:
# $date (String) - The key creation date.
openpgp-key-assistant-key-created = { $date } tarihinde oluşturuldu
