# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.


# Display Name

vcard-displayname = Görünen ad
vcard-displayname-placeholder =
    .placeholder = { vcard-displayname }

# Type selection

vcard-entry-type-label = Tür
vcard-entry-type-home = Ev
vcard-entry-type-work = İş
vcard-entry-type-none = Hiçbiri
vcard-entry-type-custom = Özel

# N vCard field

vcard-name-header = Adı
vcard-n-prefix = Ön ek
vcard-n-add-prefix =
    .title = Ön ek ekle
vcard-n-firstname = Ad
vcard-n-add-firstname =
    .title = Ad ekle
vcard-n-middlename = İkinci ad
vcard-n-add-middlename =
    .title = İkinci ad ekle
vcard-n-lastname = Soyadı
vcard-n-add-lastname =
    .title = Soyadı ekle
vcard-n-suffix = Son ek
vcard-n-add-suffix =
    .title = Son ek ekle

# Email vCard field

vcard-email-header = E-posta adresleri
vcard-email-add = E-posta adresi ekle
vcard-email-label = E-posta adresi
vcard-primary-email-label = Varsayılan

# URL vCard field

vcard-url-header = Web siteleri
vcard-url-add = Web sitesi ekle
vcard-url-label = Web sitesi

# Tel vCard field

vcard-tel-header = Telefon numaraları
vcard-tel-add = Telefon numarası ekle
vcard-tel-label = Telefon numarası

# TZ vCard field

vcard-tz-header = Saat dilimi
vcard-tz-add = Saat dilimi ekle

# IMPP vCard field

vcard-impp-header = Sohbet hesapları
vcard-impp-add = Sohbet hesabı ekle
vcard-impp-label = Sohbet hesabı

# BDAY and ANNIVERSARY vCard field

vcard-bday-anniversary-header = Özel tarihler
vcard-bday-anniversary-add = Özel tarih ekle
vcard-bday-label = Doğum günü
vcard-anniversary-label = Yıl dönümü
vcard-date-day = Gün
vcard-date-month = Ay
vcard-date-year = Yıl

# ADR vCard field

vcard-adr-header = Adresler
vcard-adr-add = Adres ekle
vcard-adr-label = Adres
vcard-adr-delivery-label = Teslimat etiketi
vcard-adr-pobox = Posta kutusu
vcard-adr-ext = Geniş adres
vcard-adr-street = Sokak adresi
# Or "Locality"
vcard-adr-locality = İlçe
# Or "Region"
vcard-adr-region = İl
# The term "ZIP code" only applies in USA. Most locales should use "Postal code" only.
vcard-adr-code = Posta kodu
vcard-adr-country = Ülke

# NOTE vCard field

vcard-note-header = Notlar
vcard-note-add = Not ekle

# TITLE, ROLE and ORGANIZATION vCard fields

vcard-org-title = Unvan
vcard-org-role = Rol
vcard-org-org = Şirket
