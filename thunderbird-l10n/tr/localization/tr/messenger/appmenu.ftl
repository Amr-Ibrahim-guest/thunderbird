# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## View / Layout

appmenu-font-size-value = Yazı tipi boyutu
appmenuitem-font-size-enlarge =
    .tooltiptext = Yazı tipi boyutunu büyüt
appmenuitem-font-size-reduce =
    .tooltiptext = Yazı tipi boyutunu küçült
# Variables:
# $size (String) - The current font size.
appmenuitem-font-size-reset =
    .label = { $size } px
    .tooltiptext = Yazı tipi boyutunu sıfırla
