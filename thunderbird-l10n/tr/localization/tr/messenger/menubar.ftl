# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

toolbar-context-menu-menu-bar =
    .toolbarname = Menü çubuğu
    .accesskey = M

## Tools Menu

menu-tools-settings =
    .label = Ayarlar
    .accesskey = A
menu-addons-and-themes =
    .label = Eklentiler ve Temalar
    .accesskey = E

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = Sorun giderme modu…
    .accesskey = o
menu-help-exit-troubleshoot-mode =
    .label = Sorun giderme modunu kapat
    .accesskey = o
menu-help-more-troubleshooting-info =
    .label = Sorun giderme bilgileri
    .accesskey = b

## Mail Toolbar

toolbar-junk-button =
    .label = Gereksiz
    .tooltiptext = Seçili iletileri gereksiz olarak işaretle
toolbar-not-junk-button =
    .label = Gereksiz değil
    .tooltiptext = Seçili iletileri gereksiz değil olarak işaretle
toolbar-delete-button =
    .label = Sil
    .tooltiptext = Seçili iletileri veya klasörü sil
toolbar-undelete-button =
    .label = Silmeyi geri al
    .tooltiptext = Seçili iletileri silmeyi geri al

## View

menu-view-repair-text-encoding =
    .label = Metin kodlamasını onar
    .accesskey = M

## View / Layout

menu-font-size-label =
    .label = Yazı tipi boyutu
    .accesskey = o
menuitem-font-size-enlarge =
    .label = Yazı tipi boyutunu büyüt
    .accesskey = b
menuitem-font-size-reduce =
    .label = Yazı tipi boyutunu küçült
    .accesskey = k
menuitem-font-size-reset =
    .label = Yazı tipi boyutunu sıfırla
    .accesskey = o
mail-uidensity-label =
    .label = Yoğunluk
    .accesskey = Y
mail-uidensity-compact =
    .label = Kompakt
    .accesskey = K
mail-uidensity-normal =
    .label = Normal
    .accesskey = N
mail-uidensity-touch =
    .label = Dokunmatik
    .accesskey = D
menu-spaces-toolbar-button =
    .label = Sekme araç çubuğu
    .accesskey = S

## File

file-new-newsgroup-account =
    .label = Haber grubu hesabı…
    .accesskey = H
