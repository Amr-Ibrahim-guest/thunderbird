# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These messages are used in the DevTools toolbox.


## These labels are shown in the "..." menu in the toolbox, and represent different
## commands such as the docking of DevTools, toggling features, and viewing some
## external links. Some of the commands have the keyboard shortcut shown next to
## the label.

toolbox-meatball-menu-dock-bottom-label = Астына бекіту
toolbox-meatball-menu-dock-left-label = Солға бекіту
toolbox-meatball-menu-dock-right-label = Оңға бекіту
toolbox-meatball-menu-dock-separate-window-label = Бөлек терезе

toolbox-meatball-menu-splitconsole-label = Бөлінген консольді көрсету
toolbox-meatball-menu-hideconsole-label = Бөлінген консольді жасыру

toolbox-meatball-menu-settings-label = Баптаулар
toolbox-meatball-menu-documentation-label = Құжаттама…
toolbox-meatball-menu-community-label = Қоғамдастық…

# This menu item is only available in the browser toolbox. It forces the popups/panels
# to stay visible on blur, which is primarily useful for addon developers and Firefox
# contributors.
toolbox-meatball-menu-noautohide-label = "Атып шығатын" терезелерді автожасыруды сөндіру

toolbox-meatball-menu-pseudo-locale-accented = "accented" локалін іске қосу
toolbox-meatball-menu-pseudo-locale-bidi = "bidi" локалін іске қосу

##

