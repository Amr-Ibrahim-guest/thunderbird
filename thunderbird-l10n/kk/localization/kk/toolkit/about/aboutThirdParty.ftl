# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

third-party-page-title = Үшінші жақты модуль ақпараты
third-party-section-title = { -brand-short-name } ішіндегі үшінші жақты модульдер тізімі

third-party-intro =
    Бұл бетте сіздің { -brand-short-name } ішіне енгізілген үшінші тарап модульдері
    көрсетіледі. Microsoft немесе { -vendor-short-name } қол қоймаған кез келген
    модуль үшінші жақты модуль болып саналады.

third-party-message-empty = Үшінші жақты модульдер табылмады.
third-party-message-no-duration = Жазылған жоқ

third-party-detail-version = Файл нұсқасы
third-party-detail-vendor = Өндіруші ақпараты
third-party-detail-occurrences = Көшірмелер
    .title = Бұл модуль неше рет жүктелген.
third-party-detail-duration = Орт. бұғаттау уақыты (мс)
    .title = Бұл модуль қолданбаны қанша уақытқа бұғаттаған.
third-party-detail-app = Қолданба
third-party-detail-publisher = Жариялаушы

third-party-th-process = Процесс
third-party-th-duration = Жүктелу ұзақтығы (мс)
third-party-th-status = Қалып-күйі

third-party-tag-ime = IME
    .title = Бұл модуль түрі сіз үшінші жақты IME қолданған кезде жүктеледі.
third-party-tag-shellex = Қоршам кеңейтуі
    .title = Бұл модуль түрі сіз файлды таңдаудың жүйелік сұхбат терезесін ашқан кезде жүктеледі.
third-party-tag-background = Фон режимі
    .title = Бұл модуль қолданбаны бұғаттамады, өйткені ол фонда жүктелген.
third-party-unsigned-icon =
    .title = Бұл модульге қол қойылмаған
third-party-warning-icon =
    .title = { -brand-short-name } бұл модуль коды салдарынан құлап түсті

third-party-status-loaded = Жүктелген
third-party-status-blocked = Бұғатталған
third-party-status-redirected = Қайта бағдарланған

third-party-button-copy-to-clipboard = Өнделмеген мәліметтерді алмасу буферіне көшіріп алу
third-party-button-reload = Жүйе ақпаратымен қайта жүктеу
    .title = Жүйе ақпаратымен қайта жүктеу
third-party-button-open =
    .title = Файл орналасуын ашу…
third-party-button-expand =
    .title = Көбірек ақпаратты көрсету
third-party-button-collapse =
    .title = Көбірек ақпаратты жасыру
