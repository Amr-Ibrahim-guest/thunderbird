# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Импорттау

## Header

import-from-app = Қолданбадан импорттау

## Buttons

button-cancel = Бас тарту

button-back = Артқа

button-continue = Жалғастыру

## Import from app steps

## Import from file selections

items-pane-checkbox-address-books = Адрестік кітапшалар

items-pane-checkbox-calendars = Күнтізбелер

items-pane-checkbox-mail-messages = Пошта хабарламалары

## Import from address book file steps

import-from-addr-book-file-desc = Импорттағыңыз келетін файл түрін таңдаңыз:

## Import from address book file steps


## Import dialog

progress-pane-restart-desc = Импорттауды аяқтау үшін қайта іске қосыңыз.

error-pane-title = Қате

## <csv-field-map> element


## Export tab

