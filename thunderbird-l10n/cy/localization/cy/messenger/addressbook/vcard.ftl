# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.


# Display Name

vcard-displayname = Enw dangos
vcard-displayname-placeholder =
    .placeholder = { vcard-displayname }

# Type selection

vcard-entry-type-label = Math
vcard-entry-type-home = Cartref
vcard-entry-type-work = Gwaith
vcard-entry-type-none = Dim
vcard-entry-type-custom = Cyfaddas

# N vCard field

vcard-name-header = Enw
vcard-n-prefix = Rhagddodiad
vcard-n-add-prefix =
    .title = Ychwanegu rhagddodiad
vcard-n-firstname = Enw cyntaf
vcard-n-add-firstname =
    .title = Ychwanegu enw cyntaf
vcard-n-middlename = Enw canol
vcard-n-add-middlename =
    .title = Ychwanegu enw canol
vcard-n-lastname = Enw olaf
vcard-n-add-lastname =
    .title = Ychwanegu enw olaf
vcard-n-suffix = Ôl-ddodiad
vcard-n-add-suffix =
    .title = Ychwanegu ôl-ddodiad

# Email vCard field

vcard-email-header = Cyfeiriadau E-bost
vcard-email-add = Ychwanegu cyfeiriad e-bost
vcard-email-label = Cyfeiriad e-bost
vcard-primary-email-label = Rhagosodiad

# URL vCard field

vcard-url-header = Gwefannau
vcard-url-add = Ychwanegu gwefan
vcard-url-label = Gwefan

# Tel vCard field

vcard-tel-header = Rhifau Ffôn
vcard-tel-add = Ychwanegu rhif ffôn
vcard-tel-label = Rhif ffôn

# TZ vCard field

vcard-tz-header = Cylch Amser
vcard-tz-add = Ychwanegu cylch amser

# IMPP vCard field

vcard-impp-header = Cyfrifon Sgwrsio
vcard-impp-add = Ychwanegu cyfrif sgwrsio
vcard-impp-label = Cyfrif sgwrsio

# BDAY and ANNIVERSARY vCard field

vcard-bday-anniversary-header = Dyddiadau Arbennig
vcard-bday-anniversary-add = Ychwanegu dyddiad arbennig
vcard-bday-label = Pen-blwydd
vcard-anniversary-label = Dathliad
vcard-date-day = Diwrnod
vcard-date-month = Mis
vcard-date-year = Blwyddyn

# ADR vCard field

vcard-adr-header = Cyfeiriadau
vcard-adr-add = Ychwanegu cyfeiriad
vcard-adr-label = Cyfeiriadau
vcard-adr-delivery-label = Label dosbarthu
vcard-adr-pobox = Blwch swyddfa'r post
vcard-adr-ext = Cyfeiriad estynedig
vcard-adr-street = Cyfeiriad stryd
# Or "Locality"
vcard-adr-locality = Tref/Dinas
# Or "Region"
vcard-adr-region = Sir/Talaith:
# The term "ZIP code" only applies in USA. Most locales should use "Postal code" only.
vcard-adr-code = Cod Post/ZIP:
vcard-adr-country = Gwlad

# NOTE vCard field

vcard-note-header = Nodiadau
vcard-note-add = Ychwanegu nodyn

# TITLE, ROLE and ORGANIZATION vCard fields

vcard-org-header = Adeiladau Sefydliadol
vcard-org-add = Ychwanegu adeiladau sefydliadol
vcard-org-title = Teitl
vcard-org-role = Rôl
vcard-org-org = Corff
