# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These messages are used in the DevTools toolbox.


## These labels are shown in the "..." menu in the toolbox, and represent different
## commands such as the docking of DevTools, toggling features, and viewing some
## external links. Some of the commands have the keyboard shortcut shown next to
## the label.

toolbox-meatball-menu-dock-bottom-label = Прикріпити знизу
toolbox-meatball-menu-dock-left-label = Прикріпити ліворуч
toolbox-meatball-menu-dock-right-label = Прикріпити праворуч
toolbox-meatball-menu-dock-separate-window-label = Окреме вікно

toolbox-meatball-menu-splitconsole-label = Показати консоль розділення
toolbox-meatball-menu-hideconsole-label = Сховати консоль розділення

toolbox-meatball-menu-settings-label = Налаштування
toolbox-meatball-menu-documentation-label = Документація…
toolbox-meatball-menu-community-label = Спільнота…

# This menu item is only available in the browser toolbox. It forces the popups/panels
# to stay visible on blur, which is primarily useful for addon developers and Firefox
# contributors.
toolbox-meatball-menu-noautohide-label = Вимкнути автоприховування спливних вікон

toolbox-meatball-menu-pseudo-locale-accented = Увімкнути “accented” локаль
toolbox-meatball-menu-pseudo-locale-bidi = Увімкнути “bidi” локаль

##

