# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## View / Layout

appmenu-font-size-value = Розмір шрифту
appmenuitem-font-size-enlarge =
    .tooltiptext = Збільшити розмір шрифту
appmenuitem-font-size-reduce =
    .tooltiptext = Зменшити розмір шрифту
# Variables:
# $size (String) - The current font size.
appmenuitem-font-size-reset =
    .label = { $size }пікселів
    .tooltiptext = Скинути розмір шрифту
