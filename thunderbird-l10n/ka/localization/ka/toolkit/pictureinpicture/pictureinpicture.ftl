# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = ეკრანი-ეკრანში
pictureinpicture-pause =
    .aria-label = შეჩერება
pictureinpicture-play =
    .aria-label = გაშვება
pictureinpicture-mute =
    .aria-label = დადუმება
pictureinpicture-unmute =
    .aria-label = ხმის ჩართვა
pictureinpicture-unpip =
    .aria-label = დაბრუნება ჩანართში
pictureinpicture-close =
    .aria-label = დახურვა
pictureinpicture-subtitles-label = ზედწარწერა
pictureinpicture-font-size-label = შრიფტის ზომა
pictureinpicture-font-size-small = მცირე
pictureinpicture-font-size-medium = საშუალო
pictureinpicture-font-size-large = დიდი
