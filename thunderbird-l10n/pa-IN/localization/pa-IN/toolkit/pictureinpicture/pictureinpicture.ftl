# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = ਤਸਵੀਰ-ਚ-ਤਸਵੀਰ
pictureinpicture-pause =
    .aria-label = ਵਿਰਾਮ
pictureinpicture-play =
    .aria-label = ਚਲਾਓ
pictureinpicture-mute =
    .aria-label = ਚੁੱਪ
pictureinpicture-unmute =
    .aria-label = ਸੁਣਾਓ
pictureinpicture-unpip =
    .aria-label = ਟੈਬ ਤੇ ਵਾਪਸ ਭੇਜੋ
pictureinpicture-close =
    .aria-label = ਬੰਦ ਕਰੋ
pictureinpicture-subtitles-label = ਸਬ-ਟਾਈਟਲ
pictureinpicture-font-size-label = ਫ਼ੋਂਟ ਦਾ ਆਕਾਰ
pictureinpicture-font-size-small = ਛੋਟੇ
pictureinpicture-font-size-medium = ਠੀਕ-ਠਾਕ
pictureinpicture-font-size-large = ਵੱਡੇ
