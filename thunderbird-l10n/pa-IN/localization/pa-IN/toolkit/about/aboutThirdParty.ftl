# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

third-party-page-title = ਸੁਤੰਤਰ-ਧਿਰ ਮੋਡੀਊਲ ਜਾਣਕਾਰੀ
third-party-section-title = { -brand-short-name } ਵਿੱਚ ਸੁਤੰਤਰ ਧਿਰ ਦੇ ਮੋਡੀਊਲਾਂ ਦੀ ਸੂਚੀ
third-party-message-empty = ਕੋਈ ਵੀ ਸੁਤੰਤਰ ਧਿਰ ਦਾ ਮੋਡੀਊਲ ਖੋਜਿਆ ਨਹੀਂ ਗਿਆ।
third-party-message-no-duration = ਰਿਕਾਰਡ ਨਹੀਂ ਹੈ
third-party-detail-version = ਫ਼ਾਈਲ ਵਰਜ਼ਨ
third-party-detail-vendor = ਵੇਂਡਰ ਜਾਣਕਾਰੀ
third-party-detail-occurrences = ਮੌਜੂਦਗੀਆਂ
    .title = ਇਹ ਮੋਡੀਊਲ ਕਿੰਨੀ ਵਾਰ ਲੋਡ ਕੀਤਾ ਗਿਆ ਸੀ
third-party-detail-app = ਐਪਲੀਕੇਸ਼ਨ
third-party-detail-publisher = ਪ੍ਰਕਾਸ਼ਕ
third-party-th-process = ਪਰੋਸੈਸ
third-party-th-duration = ਲੋਡ ਹੋਣ ਦਾ ਸਮਾਂ (ms)
third-party-th-status = ਹਾਲਤ
third-party-status-loaded = ਲੋਡ ਹੈ
third-party-status-blocked = ਪਾਬੰਦੀ ਲਗਾਏ
third-party-status-redirected = ਰਿ-ਡਾਇਰੈਕਟ
third-party-button-copy-to-clipboard = ਰਾਅ ਡਾਟਾ ਕਲਿੱਪਬੋਰਡ ਵਿੱਚ ਕਾਪੀ ਕਰੋ
third-party-button-reload = ਸਿਸਟਮ ਜਾਣਕਾਰੀ ਨਾਲ ਮੁੜ ਲੋਡ ਕਰੋ
    .title = ਸਿਸਟਮ ਜਾਣਕਾਰੀ ਨਾਲ ਮੁੜ ਲੋਡ ਕਰੋ
third-party-button-open =
    .title = …ਫਾਇਲ ਟਿਕਾਣੇ ਨੂੰ ਖੋਲ੍ਹੋ
third-party-button-expand =
    .title = ਵੇਰਵੇ ਸਮੇਤ ਜਾਣਕਾਰੀ ਵੇਖਾਓ
third-party-button-collapse =
    .title = ਵੇਰਵੇ ਸਮੇਤ ਜਾਣਕਾਰੀ ਨੂੰ ਸਮੇਟੋ
