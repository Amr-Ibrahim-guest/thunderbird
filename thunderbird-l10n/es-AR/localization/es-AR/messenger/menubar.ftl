# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

toolbar-context-menu-menu-bar =
    .toolbarname = Barra de menú
    .accesskey = m

## Tools Menu

menu-tools-settings =
    .label = Ajustes
    .accesskey = e
menu-addons-and-themes =
    .label = Complementos y temas
    .accesskey = a

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = Modo de resolución de problemas…
    .accesskey = b
menu-help-exit-troubleshoot-mode =
    .label = Desactivar el modo de resolución de problemas
    .accesskey = O
menu-help-more-troubleshooting-info =
    .label = Más información para solucionar problemas
    .accesskey = M

## Mail Toolbar

toolbar-junk-button =
    .label = Basura
    .tooltiptext = Marcar los mensajes seleccionados como basura
toolbar-not-junk-button =
    .label = No es basura
    .tooltiptext = Marcar los mensajes seleccionados como no basura
toolbar-delete-button =
    .label = Borrar
    .tooltiptext = Borrar los mensajes o carpetas seleccionados
toolbar-undelete-button =
    .label = Recuperar
    .tooltiptext = Recuperar los mensajes seleccionados

## View

menu-view-repair-text-encoding =
    .label = Reparar codificación de texto
    .accesskey = c

## View / Layout

menu-font-size-label =
    .label = Tamaño de letra
    .accesskey = o
menuitem-font-size-enlarge =
    .label = Aumentar tamaño del texto
    .accesskey = u
menuitem-font-size-reduce =
    .label = Reducir tamaño del texto
    .accesskey = d
menuitem-font-size-reset =
    .label = Restablecer tamaño del texto
    .accesskey = R
mail-uidensity-label =
    .label = Densidad
    .accesskey = D
mail-uidensity-compact =
    .label = Compacto
    .accesskey = C
mail-uidensity-normal =
    .label = Normal
    .accesskey = N
mail-uidensity-touch =
    .label = Táctil
    .accesskey = T
menu-spaces-toolbar-button =
    .label = Barra de herramientas de Espacios
    .accesskey = S

## File

file-new-newsgroup-account =
    .label = Cuenta de grupo de noticias…
    .accesskey = n
