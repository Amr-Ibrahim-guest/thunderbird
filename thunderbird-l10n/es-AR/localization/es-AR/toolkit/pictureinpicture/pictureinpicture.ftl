# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Picture-in-picture
pictureinpicture-pause =
    .aria-label = Pausa
pictureinpicture-play =
    .aria-label = Reproducir
pictureinpicture-mute =
    .aria-label = Mudo
pictureinpicture-unmute =
    .aria-label = Activar audio
pictureinpicture-unpip =
    .aria-label = Enviar de vuelta a la pestaña
pictureinpicture-close =
    .aria-label = Cerrar
pictureinpicture-subtitles-label = Subtítulos
pictureinpicture-font-size-label = Tamaño de letra
pictureinpicture-font-size-small = Chico
pictureinpicture-font-size-medium = Mediano
pictureinpicture-font-size-large = Grande
