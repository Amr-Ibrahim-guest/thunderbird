# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These messages are used in the DevTools toolbox.


## These labels are shown in the "..." menu in the toolbox, and represent different
## commands such as the docking of DevTools, toggling features, and viewing some
## external links. Some of the commands have the keyboard shortcut shown next to
## the label.

toolbox-meatball-menu-dock-bottom-label = Zasidraj na dno
toolbox-meatball-menu-dock-left-label = Zasidraj na levo
toolbox-meatball-menu-dock-right-label = Zasidraj na desno
toolbox-meatball-menu-dock-separate-window-label = Ločeno okno

toolbox-meatball-menu-splitconsole-label = Prikaži deljeno konzolo
toolbox-meatball-menu-hideconsole-label = Skrij deljeno konzolo

toolbox-meatball-menu-settings-label = Nastavitve
toolbox-meatball-menu-documentation-label = Dokumentacija …
toolbox-meatball-menu-community-label = Skupnost …

# This menu item is only available in the browser toolbox. It forces the popups/panels
# to stay visible on blur, which is primarily useful for addon developers and Firefox
# contributors.
toolbox-meatball-menu-noautohide-label = Onemogoči samodejno skrivanje pojavnih oken

##

