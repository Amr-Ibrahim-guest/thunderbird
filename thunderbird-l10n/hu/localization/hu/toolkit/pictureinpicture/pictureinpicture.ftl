# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Kép a képben
pictureinpicture-pause =
    .aria-label = Szünet
pictureinpicture-play =
    .aria-label = Lejátszás
pictureinpicture-mute =
    .aria-label = Némítás
pictureinpicture-unmute =
    .aria-label = Hang be
pictureinpicture-unpip =
    .aria-label = Visszaküldés a lapra
pictureinpicture-close =
    .aria-label = Bezárás
pictureinpicture-subtitles-label = Feliratok
pictureinpicture-font-size-label = Betűméret
pictureinpicture-font-size-small = Kicsi
pictureinpicture-font-size-medium = Közepes
pictureinpicture-font-size-large = Nagy
