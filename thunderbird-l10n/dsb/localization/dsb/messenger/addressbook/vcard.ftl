# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.


# Display Name

vcard-displayname = Zwobraznjeńske mě
vcard-displayname-placeholder =
    .placeholder = { vcard-displayname }

# Type selection

vcard-entry-type-label = Typ
vcard-entry-type-home = Startowy bok
vcard-entry-type-work = Słužbny telefon
vcard-entry-type-none = Žeden
vcard-entry-type-custom = Swójski

# N vCard field

vcard-name-header = Mě
vcard-n-prefix = Prefiks
vcard-n-add-prefix =
    .title = Prefiks pśidaś
vcard-n-firstname = Pśedmě
vcard-n-add-firstname =
    .title = Pśedmě pśidaś
vcard-n-middlename = Druge pśedmě
vcard-n-add-middlename =
    .title = Druge pśedmě pśidaś
vcard-n-lastname = Familijowe mě
vcard-n-add-lastname =
    .title = Familijowe mě pśidaś
vcard-n-suffix = Sufiks
vcard-n-add-suffix =
    .title = Sufiks pśidaś

# Email vCard field

vcard-email-header = E-mailowe adrese
vcard-email-add = E-mailowu adresu pśidaś
vcard-email-label = E-mailowa adresa
vcard-primary-email-label = Standard

# URL vCard field

vcard-url-header = Websedła
vcard-url-add = Websedło pśidaś
vcard-url-label = Websedło

# Tel vCard field

vcard-tel-header = Telefonowe numery
vcard-tel-add = Telefonowy numer pśidaś
vcard-tel-label = Telefonowy numer

# TZ vCard field

vcard-tz-header = Casowa cona
vcard-tz-add = Casowu conu pśidaś

# IMPP vCard field

vcard-impp-header = Chatowe konta
vcard-impp-add = Chatowe konto pśidaś
vcard-impp-label = Chatowe konto

# BDAY and ANNIVERSARY vCard field

vcard-bday-anniversary-header = Specialne datumy
vcard-bday-anniversary-add = Specialny datum pśidaś
vcard-bday-label = Narodny źeń
vcard-anniversary-label = Wrośenica
vcard-date-day = Źeń
vcard-date-month = Mjasec
vcard-date-year = Lěto

# ADR vCard field

vcard-adr-header = Adrese
vcard-adr-add = Adresu pśidaś
vcard-adr-label = Adresa
vcard-adr-delivery-label = Librowański etiket
vcard-adr-pobox = Postowy fach
vcard-adr-ext = Rozšyrjona adresa
vcard-adr-street = Drogowa adresa
# Or "Locality"
vcard-adr-locality = Město
# Or "Region"
vcard-adr-region = Zwězkowy kraj
# The term "ZIP code" only applies in USA. Most locales should use "Postal code" only.
vcard-adr-code = Postowa licba
vcard-adr-country = Kraj

# NOTE vCard field

vcard-note-header = Pśipiski
vcard-note-add = Pśipisk pśidaś

# TITLE, ROLE and ORGANIZATION vCard fields

vcard-org-header = Organizaciske kakosći
vcard-org-add = Organizaciske kakosći pśidaś
vcard-org-title = Titel
vcard-org-role = Rola
vcard-org-org = Organizacija
