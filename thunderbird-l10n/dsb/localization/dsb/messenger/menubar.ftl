# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

toolbar-context-menu-menu-bar =
    .toolbarname = Menijowa rědka
    .accesskey = M

## Tools Menu

menu-tools-settings =
    .label = Nastajenja
    .accesskey = N
menu-addons-and-themes =
    .label = Dodanki a drastwy
    .accesskey = D

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = Modus za rozwězowanje problemow…
    .accesskey = M
menu-help-exit-troubleshoot-mode =
    .label = Modus za rozwězowanje problemow znjemóžniś
    .accesskey = r
menu-help-more-troubleshooting-info =
    .label = Dalšne informacije za rozwězowanje problemow
    .accesskey = D

## Mail Toolbar

toolbar-junk-button =
    .label = Cajk
    .tooltiptext = Wubrane powěsći ako cajk markěrowaś
toolbar-not-junk-button =
    .label = Njecajk
    .tooltiptext = Powěsći ako njecajk markěrowaś
toolbar-delete-button =
    .label = Lašowaś
    .tooltiptext = Wubrane powěsći abo zarědnik lašowaś
toolbar-undelete-button =
    .label = Wótnowiś
    .tooltiptext = Wubrane powěsći wótnowiś

## View

menu-view-repair-text-encoding =
    .label = Tekstowe koděrowanje reparěrowaś
    .accesskey = T

## View / Layout

menu-font-size-label =
    .label = Pismowe wjelikosć
    .accesskey = i
menuitem-font-size-enlarge =
    .label = Pismowu wjelikosć pówětšyś
    .accesskey = m
menuitem-font-size-reduce =
    .label = Pismowu wjelikosć pómjeńšyś
    .accesskey = l
menuitem-font-size-reset =
    .label = Pismowu wjelikosć slědk stajiś
    .accesskey = d
mail-uidensity-label =
    .label = Gustosć
    .accesskey = G
mail-uidensity-compact =
    .label = Kompaktny
    .accesskey = K
mail-uidensity-normal =
    .label = Normalny
    .accesskey = N
mail-uidensity-touch =
    .label = Dotyknjenje
    .accesskey = D
menu-spaces-toolbar-button =
    .label = Rědka źělnych programow
    .accesskey = l

## File

file-new-newsgroup-account =
    .label = Konto za diskusijnu kupku…
    .accesskey = d
