# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = 子母畫面
pictureinpicture-pause =
    .aria-label = 暫停
pictureinpicture-play =
    .aria-label = 播放
pictureinpicture-mute =
    .aria-label = 靜音
pictureinpicture-unmute =
    .aria-label = 取消靜音
pictureinpicture-unpip =
    .aria-label = 送回分頁
pictureinpicture-close =
    .aria-label = 關閉
pictureinpicture-subtitles-label = 字幕
pictureinpicture-font-size-label = 字型大小
pictureinpicture-font-size-small = 小
pictureinpicture-font-size-medium = 中
pictureinpicture-font-size-large = 大
