# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Incrustation vidéo
pictureinpicture-pause =
    .aria-label = Pause
pictureinpicture-play =
    .aria-label = Lecture
pictureinpicture-mute =
    .aria-label = Muet
pictureinpicture-unmute =
    .aria-label = Audible
pictureinpicture-unpip =
    .aria-label = Renvoyer dans l’onglet
pictureinpicture-close =
    .aria-label = Fermer
pictureinpicture-subtitles-label = Sous-titres
pictureinpicture-font-size-label = Taille de police
pictureinpicture-font-size-small = Petite
pictureinpicture-font-size-medium = Moyenne
pictureinpicture-font-size-large = Grande
