# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

toolbar-context-menu-menu-bar =
    .toolbarname = Menijowa lajsta
    .accesskey = M

## Tools Menu

menu-tools-settings =
    .label = Nastajenja
    .accesskey = N
menu-addons-and-themes =
    .label = Přidatki a drasty
    .accesskey = P

## Help Menu

menu-help-enter-troubleshoot-mode =
    .label = Modus za rozrisowanje problemow…
    .accesskey = M
menu-help-exit-troubleshoot-mode =
    .label = Modus za rozrisanje problemow znjemóžnić
    .accesskey = m
menu-help-more-troubleshooting-info =
    .label = Dalše informacije za rozrisowanje problemow
    .accesskey = D

## Mail Toolbar

toolbar-junk-button =
    .label = Čapor
    .tooltiptext = Wubrane powěsće jako čapor markěrować
toolbar-not-junk-button =
    .label = Nječapor
    .tooltiptext = Wubrane powěsće jako nječapor markěrować
toolbar-delete-button =
    .label = Zhašeć
    .tooltiptext = Wubrane powěsće abo rjadowak zhašeć
toolbar-undelete-button =
    .label = Wobnowić
    .tooltiptext = Wubrane powěsće wobnowić

## View

menu-view-repair-text-encoding =
    .label = Tekstowe kodowanje reparować
    .accesskey = d

## View / Layout

menu-font-size-label =
    .label = Pismowa wulkosć
    .accesskey = i
menuitem-font-size-enlarge =
    .label = Pismowu wulkosć powjetšić
    .accesskey = w
menuitem-font-size-reduce =
    .label = Pismowu wulkosć pomjeńšić
    .accesskey = m
menuitem-font-size-reset =
    .label = Pismowu wulkosć wróćo stajić
    .accesskey = r
mail-uidensity-label =
    .label = Hustosć
    .accesskey = H
mail-uidensity-compact =
    .label = Kompaktny
    .accesskey = K
mail-uidensity-normal =
    .label = Normalny
    .accesskey = N
mail-uidensity-touch =
    .label = Dótk
    .accesskey = D
menu-spaces-toolbar-button =
    .label = Lajsta dźělnych programow
    .accesskey = d

## File

file-new-newsgroup-account =
    .label = Konto za diskusijnu skupinu…
    .accesskey = d
