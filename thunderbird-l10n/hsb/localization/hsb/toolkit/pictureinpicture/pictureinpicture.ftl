# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Wobraz-we-wobrazu
pictureinpicture-pause =
    .aria-label = Přestawka
pictureinpicture-play =
    .aria-label = Wothrać
pictureinpicture-mute =
    .aria-label = Bjez zynka
pictureinpicture-unmute =
    .aria-label = Ze zynkom
pictureinpicture-unpip =
    .aria-label = K rajtarkej wróćo pósłać
pictureinpicture-close =
    .aria-label = Začinić
pictureinpicture-subtitles-label = Podtitule
pictureinpicture-font-size-label = Pismowa wulkosć
pictureinpicture-font-size-small = Mały
pictureinpicture-font-size-medium = Srjedźny
pictureinpicture-font-size-large = Wulki
