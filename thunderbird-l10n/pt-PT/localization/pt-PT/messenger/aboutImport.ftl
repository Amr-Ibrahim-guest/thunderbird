# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importar
export-page-title = Exportar

## Header

export-profile = Exportar

## Buttons

button-back = Retroceder
button-continue = Continuar
button-export = Exportar

## Import from app steps

app-name-thunderbird = Thunderbird
app-name-seamonkey = SeaMonkey
app-name-outlook = Outlook
app-name-becky = Becky! Internet Mail
app-name-apple-mail = Correio Apple

## Import from file selections

file-calendar = Importar calendários

## Import from app profile steps

items-pane-checkbox-mail-messages = Mensagens eletrónicas

## Import from address book file steps


## Import from calendar file steps


## Import dialog

error-pane-title = Erro

## <csv-field-map> element


## Export tab

export-brand-name = { -brand-product-name }

## Summary pane


## Footer area


## Step navigation on top of the wizard pages

