# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Bideoa beste leiho batean
pictureinpicture-pause =
    .aria-label = Pausatu
pictureinpicture-play =
    .aria-label = Erreproduzitu
pictureinpicture-mute =
    .aria-label = Mututu
pictureinpicture-unmute =
    .aria-label = Ez mututu
pictureinpicture-unpip =
    .aria-label = Bidali berriro fitxara
pictureinpicture-close =
    .aria-label = Itxi
pictureinpicture-subtitles-label = Azpitituluak
pictureinpicture-font-size-label = Letra-tamaina
pictureinpicture-font-size-small = Txikia
pictureinpicture-font-size-medium = Ertaina
pictureinpicture-font-size-large = Handia
