# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These messages are used in the DevTools toolbox.


## These labels are shown in the "..." menu in the toolbox, and represent different
## commands such as the docking of DevTools, toggling features, and viewing some
## external links. Some of the commands have the keyboard shortcut shown next to
## the label.

toolbox-meatball-menu-dock-bottom-label = Nguleni poshtë
toolbox-meatball-menu-dock-left-label = Vendose Majtas
toolbox-meatball-menu-dock-right-label = Vendose Djathtas
toolbox-meatball-menu-dock-separate-window-label = Dritare veçmas

toolbox-meatball-menu-splitconsole-label = Shfaq konsol ndarjesh
toolbox-meatball-menu-hideconsole-label = Fshih konsol ndarjesh

toolbox-meatball-menu-settings-label = Rregullime
toolbox-meatball-menu-documentation-label = Dokumentim…
toolbox-meatball-menu-community-label = Bashkësi…

# This menu item is only available in the browser toolbox. It forces the popups/panels
# to stay visible on blur, which is primarily useful for addon developers and Firefox
# contributors.
toolbox-meatball-menu-noautohide-label = Çaktivizo vetëfshehje flluskash

##

