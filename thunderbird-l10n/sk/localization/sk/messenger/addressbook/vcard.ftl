# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.


# Display Name

vcard-displayname = Zobrazované meno
vcard-displayname-placeholder =
    .placeholder = { vcard-displayname }

# Type selection

vcard-entry-type-label = Typ
vcard-entry-type-home = Domov
vcard-entry-type-work = Práca
vcard-entry-type-none = Neurčené
vcard-entry-type-custom = Vlastné

# N vCard field

vcard-name-header = Meno
vcard-n-prefix = Titul pred menom
vcard-n-add-prefix =
    .title = Pridať titul pred menom
vcard-n-firstname = Meno
vcard-n-add-firstname =
    .title = Pridať krstné meno
vcard-n-middlename = Stredné meno
vcard-n-add-middlename =
    .title = Pridať stredné meno
vcard-n-lastname = Priezvisko
vcard-n-add-lastname =
    .title = Pridať priezvisko
vcard-n-suffix = Titul za menom
vcard-n-add-suffix =
    .title = Pridať titul za menom

# Email vCard field

vcard-email-header = E-mailové adresy
vcard-email-add = Pridať e-mailovú adresu
vcard-email-label = E-mailová adresa
vcard-primary-email-label = Predvolené

# URL vCard field

vcard-url-header = Webové stránky
vcard-url-add = Pridať webovú stránku
vcard-url-label = Webová stránka

# Tel vCard field

vcard-tel-header = Telefónne čísla
vcard-tel-add = Pridať telefónne číslo
vcard-tel-label = Telefónne číslo

# TZ vCard field

vcard-tz-header = Časové pásmo
vcard-tz-add = Pridať časové pásmo

# IMPP vCard field

vcard-impp-header = Účty pre konverzácie
vcard-impp-add = Pridať účet pre konverzácie
vcard-impp-label = Účet pre konverzácie

# BDAY and ANNIVERSARY vCard field

vcard-bday-anniversary-header = Špeciálne dátumy
vcard-bday-anniversary-add = Pridať špeciálny dátum
vcard-bday-label = Narodeniny
vcard-anniversary-label = Výročie
vcard-date-day = Deň
vcard-date-month = Mesiac
vcard-date-year = Rok

# ADR vCard field

vcard-adr-header = Adresy
vcard-adr-add = Pridať adresu
vcard-adr-label = Adresa
vcard-adr-delivery-label = Doručovací štítok
vcard-adr-pobox = Poštová schránka
vcard-adr-ext = Rozšírená adresa
vcard-adr-street = Ulica
# Or "Locality"
vcard-adr-locality = Mesto
# Or "Region"
vcard-adr-region = Štát alebo provincia
# The term "ZIP code" only applies in USA. Most locales should use "Postal code" only.
vcard-adr-code = PSČ
vcard-adr-country = Krajina

# NOTE vCard field

vcard-note-header = Poznámky
vcard-note-add = Pridať poznámku

# TITLE, ROLE and ORGANIZATION vCard fields

vcard-org-header = Pozícia vo firme
vcard-org-add = Pridať pozíciu vo firme
vcard-org-title = Titul
vcard-org-role = Funkcia
vcard-org-org = Organizácia
