# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Picture-in-Picture

pictureinpicture-pause =
    .aria-label = Pausa
pictureinpicture-play =
    .aria-label = Riproduci

pictureinpicture-mute =
    .aria-label = Disattiva audio
pictureinpicture-unmute =
    .aria-label = Attiva audio

pictureinpicture-unpip =
  .aria-label = Rimanda alla scheda

pictureinpicture-close =
  .aria-label = Chiudi

pictureinpicture-subtitles-label = Sottotitoli

pictureinpicture-font-size-label = Dimensione carattere

pictureinpicture-font-size-small = Piccola

pictureinpicture-font-size-medium = Media

pictureinpicture-font-size-large = Grande
